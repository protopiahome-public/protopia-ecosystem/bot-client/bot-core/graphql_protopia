<?php

function get_morphy() {
 require_once(__DIR__ . "/vendor/cijic/phpmorphy/libs/phpmorphy/src/common.php");
 $dir = __DIR__ . "/vendor/cijic/phpmorphy/libs/phpmorphy/dicts";
 $lang = 'ru_RU';
 $opts = array(
  'storage' => PHPMORPHY_STORAGE_SHM,
 );
 $morphy = new phpMorphy($dir, $lang, $opts);
 return $morphy;
}

function lemmatize($word) {
 preg_match("/[а-яё]+[а-яё-]*/isu", $word, $matches);
    $result = get_morphy()->lemmatize(mb_strtoupper($matches[0]))[0];
    if (!$result) {
        $result = $word;
    }
    return $result;
}
